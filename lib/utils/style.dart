import 'package:flutter/material.dart';

class StyleApp{

  static Color colorPrimary = Color(0xff07213B);
  static Color colorWhite = Color(0xffffffff);
  static Color colorGreen = Color(0xff3B7E7E);
  static Color colorRed = Color(0xfffc0345);

  static TextStyle title =TextStyle(fontSize: 24, fontWeight: FontWeight.bold, fontFamily: "OpenSans");
  static TextStyle subTitle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold, fontFamily: "OpenSans");
}