import 'package:flutter/material.dart';
import 'package:kawalcovid/view/homeViewPage.dart';

void main() => runApp(Home());

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
title: 'Kawal Covid-19',
theme: ThemeData(
primaryColor: Colors.blueAccent
),
      home: HomeViewPage(),
    );
  }
}


